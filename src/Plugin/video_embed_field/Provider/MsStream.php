<?php

/**
 * @file
 * Contains \Drupal\video_embed_ms_stream\Plugin\video_embed_field\Provider\MsStream.
 */

namespace Drupal\video_embed_ms_stream\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * An MS Stream provider plugin.
 *
 * @VideoEmbedProvider(
 *   id = "ms_stream",
 *   title = @Translation("MS Stream")
 * )
 */
class MsStream extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $iframe = [
      '#type' => 'video_embed_iframe',
      '#provider' => 'ms_stream',
      '#url' => sprintf('https://web.microsoftstream.com/embed/video/%s', $this->getVideoId()),
      '#query' => [
        'autoplay' => $autoplay,
      ],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
    return $iframe;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return $this->oEmbedData()->thumbnail_url;
  }

  /**
   * Get MS Stream oembed data.
   *
   * @return array
   *   An array of data from the oembed endpoint.
   */
  protected function oEmbedData() {
    return json_decode(file_get_contents('https://web.microsoftstream.com/oembed?url=' . $this->getInput()));
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    preg_match('/^https?:\/\/(www\.)?([a-zA-Z0-9-]*)?.microsoftstream.com\/(embed\/)?video\/(?<id>[a-zA-Z0-9-]*)/', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }

}
